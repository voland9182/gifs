import './App.css';
import axios from 'axios';
import React from 'react';

const apiKey ='WiHFWMPwMlbIvoXL8ecrhQcfe5n8HK9q'
const apiURI='https://api.giphy.com/v1/gifs/search'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoader: false,
            items: [],
            value: '',
            offset: 0,
            isDisabled: true
        };

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.downloadYet = this.downloadYet.bind(this)
    }
    handleSubmit(){
        this.setState ({
            offset: 0,
        })
        const params = {
            api_key: apiKey,
            q: this.state.value,
            offset: 0
        }

        this.sendRequest(params)
    }

    sendRequest(params) {
        this.setState({
            isLoader: true,
        })
        axios.get(apiURI, {params}).then(response => {
            console.log(response.data);
            this.setState({
                isLoader: false,
                items: response.data.data

            })
        }).catch(error => {
            this.setState({
                isLoader: false,
                error: error
            })
        });
    }

    handleChange(event) {
        this.setState({
            value: event.target.value,
            isDisabled: event.target.value.length <= 0
        })
    }

    downloadYet() {

        let offset = this.state.offset  + 50
        this.setState ({
            offset: offset,
        })

        const params = {
            api_key: apiKey,
            q: this.state.value,
            offset: offset
        }
        this.sendRequest(params)

    }

    render() {
        const { error, isLoader, items, isDisabled } = this.state;
        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoader) {
            return (
                <div className="App">
                    <header className="App-header">
                        <div className="App">
                            <label className="text" htmlFor="text">
                                Введите название гифки:
                            </label>
                            <input onChange={this.handleChange} value={this.state.value} type='text' id='text'/>
                            <button disabled={isDisabled} className="btn" type="button" onClick={this.handleSubmit}> Отправить</button>
                        </div>

                    </header>
                    <button className="btn" type="button" onClick={this.downloadYet } > Загрузить еще</button>
                    <div>
                        Инфа о гифках
                    </div>
                    <div className="ImageHolder">
                    <div>
                        <h1> {isDisabled}</h1>
                        <ul>
                        {items.length > 0 ? items.map(item => <li>
                            <img
                                onMouseOver={(event) => { event.target.src = item.images.original.url }}
                                onMouseOut={(event) => { event.target.src = item.images['480w_still'].url }}
                                src ={item.images['480w_still'].url} alt="image"/>
                        </li>)
                            :
                            <h2>GIF - изображения не найдены!</h2>}
                        </ul>
                </div>
                </div>
                </div>


        )
        } else {
            return (
               <div>Подождите идет загрузка..</div>
                )
        }


    }



}




export default App
/*function GetGifts() {
    const [appState, setAppState] =useState();
    useEffect(() => {
        axios.get(apiURI).then((resp) => {
            // axios.get(apiURI).then((resp) => {
            const allPersons = resp.data;
            setAppState(allPersons);

        })
    }, [setAppState]);
}*/
/*

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>*/
